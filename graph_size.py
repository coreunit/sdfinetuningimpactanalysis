import json
from pathlib import Path
import matplotlib.pyplot as plt

def load_json_and_plot_distribution(file_path, save_path=None):
    with open(file_path, 'r') as file:
        data = json.load(file)

    sizes_x = [image_data['size'][0] for image_data in data.values()]
    sizes_y = [image_data['size'][1] for image_data in data.values()]

    plt.scatter(sizes_x, sizes_y, color='blue', alpha=0.5)
    plt.title('2D Distribution of Image Sizes')
    plt.xlabel('Width')
    plt.ylabel('Height')

    if save_path:
        file_name = Path(file_path).stem
        save_file_path = Path(save_path) / f'{file_name}_size_distribution.png'
        plt.savefig(save_file_path)
        print(f"Graph saved at: {save_file_path}")
    else:
        plt.show()

if __name__ == "__main__":
    json_dir = Path('data/imagenet-1k')
    output_dir = Path('output')

    output_dir.mkdir(exist_ok=True)

    for file_path in json_dir.glob('*.json'):
        load_json_and_plot_distribution(file_path, save_path=output_dir)