import torch
import diffusers

config = {
	"sd-v1-5": {
		"name": "sd-v1-5",
		"model": "v1-5-pruned.safetensors",
		"pipe": diffusers.StableDiffusionPipeline,
		"width": 512,
		"height": 512,
		"guidance_scale": [7.0],
		"num_inference_steps": range(16,64),
		"scheduler": [
			diffusers.EulerDiscreteScheduler,
			diffusers.EulerAncestralDiscreteScheduler,
			diffusers.DPMSolverSDEScheduler,
			diffusers.DPMSolverMultistepScheduler
		],
		"base": [
			{
				"url": "https://huggingface.co/runwayml/stable-diffusion-v1-5/blob/main/v1-5-pruned.safetensors",
				"file": "v1-5-pruned.safetensors"
			}
		],
		"loras": [
			{
				"url": "https://civitai.com/models/58390?modelVersionId=62833",
				"file": "add_detail.safetensors",
				"prompt": "add_detail"
			},
			{
				"url": "https://civitai.com/models/82098?modelVersionId=87153",
				"file": "more_details.safetensors",
				"prompt": "more_details"
			},
			{
				"url": "https://civitai.com/models/13941?modelVersionId=16576",
				"file": "epi_noiseoffset2.safetensors",
				"prompt": "dark studio, rim lighting, two tone lighting, dimly lit, low key"
			},
			{
				"url": "https://civitai.com/models/110334?modelVersionId=118945",
				"file": "epiCRealismHelper.safetensors",
				"prompt": ""
			},
			{
				"url": "https://civitai.com/models/12597?modelVersionId=14856",
				"file": "MoXinV1.safetensors",
				"prompt": "shuimobysim, wuchangshuo, bonian, zhenbanqiao, badashanren"
			},
			{
				"url": "https://civitai.com/models/25995?modelVersionId=32988",
				"file": "blindbox_v1_mix.safetensors",
				"prompt": "full body, chibi"
			},
			{
				"url": "https://civitai.com/models/147751?modelVersionId=164807",
				"file": "DarkLighting.safetensors",
				"prompt": "silhouette, spotlight, dark theme"
			},
			{
				"url": "https://civitai.com/models/140993?modelVersionId=156286",
				"file": "epiCRealLife.safetensors",
				"prompt": "epiCRealLife"
			}
		],
		"checkpoints" : [
			{
				"url": "https://civitai.com/models/4201?modelVersionId=245598",
				"file": "realisticVisionV60B1_v60B1VAE.safetensors"
			},
			{
				"url": "https://civitai.com/models/4384?modelVersionId=128713",
				"file": "dreamshaper_8.safetensors"
			},
			{
				"url": "https://civitai.com/models/25694?modelVersionId=160989",
				"file": "epicrealism_naturalSin.safetensors"
			},
			{
				"url": "https://civitai.com/models/132632?modelVersionId=223670",
				"file": "epicphotogasm_lastUnicorn.safetensors"
			},
			{
				"url": "https://civitai.com/models/81458/modelVersionId=132760",
				"file": "absolutereality_v181.safetensors"
			},
			{
				"url": "https://civitai.com/models/28059?modelVersionId=253668",
				"file": "icbinpICantBelieveIts_lcm.safetensors"
			}
		]
	},
	"sd-v2-1": {
		"name": "sd-v2-1",
		"model": "v2-1_768-ema-pruned.safetensors",
		"pipe": diffusers.StableDiffusionPipeline,
		"width": 768,
		"height": 768,
		"guidance_scale": [7.0],
		"num_inference_steps": range(16,48),
		"scheduler": [
			diffusers.EulerDiscreteScheduler,
			diffusers.EulerAncestralDiscreteScheduler,
			diffusers.DPMSolverSDEScheduler,
			diffusers.DPMSolverMultistepScheduler
		],
		"base": [
			{
				"url": "https://huggingface.co/stabilityai/stable-diffusion-2-1/blob/main/v2-1_768-ema-pruned.safetensors",
				"file": "v2-1_768-ema-pruned.safetensors"
			}
		],
		"loras": [

		],
		"checkpoints" : [
			{
				"url": "",
				"file": ""
			},
			{
				"url": "",
				"file": ""
			},
			{
				"url": "",
				"file": ""
			},
			{
				"url": "",
				"file": ""
			},
			{
				"url": "",
				"file": ""
			},
			{
				"url": "",
				"file": ""
			}
		]
	},
	"sd-xl-base-v1-0": {
		"name": "sd-xl-base-v1-0",
		"model": "sd_xl_base_1.0.safetensors",
		"pipe": diffusers.StableDiffusionXLPipeline,
		"width": 1024,
		"height": 1024,
		"guidance_scale": [7.0],
		"num_inference_steps": range(16,32),
		"scheduler": [
			diffusers.EulerDiscreteScheduler,
			diffusers.EulerAncestralDiscreteScheduler,
			diffusers.DPMSolverSDEScheduler,
			diffusers.DPMSolverMultistepScheduler
		],
		"base": [
			{
				"url": "https://huggingface.co/stabilityai/stable-diffusion-xl-base-1.0/blob/main/sd_xl_base_1.0.safetensors",
				"file": "sd_xl_base_1.0.safetensors"
			}
		],
		"loras": [
			{
				"url": "https://civitai.com/models/124347?modelVersionId=141133",
				"file": "xl_real_beta1.safetensors"
			},
			{
				"url": "https://civitai.com/models/122359?modelVersionId=135867",
				"file": "add-detail-xl.safetensors"
			},
			{
				"url": "https://civitai.com/models/120663?modelVersionId=131991",
				"file": "JuggerCineXL2.safetensors"
			}
		],
		"checkpoints" : [
			{
				"url": "https://civitai.com/models/139562?modelVersionId=268861",
				"file": "realvisxlV30_v30Bakedvae.safetensors"
			},
			{
				"url": "https://civitai.com/models/133005?modelVersionId=240840",
				"file": "juggernautXL_v7Rundiffusion.safetensors"
			},
			{
				"url": "https://civitai.com/models/128607?modelVersionId=247444",
				"file": "nightvisionXLPhotorealisticPortrait_v0791Bakedvae.safetensors"
			},
			{
				"url": "https://civitai.com/models/140737?modelVersionId=238308",
				"file": "albedobaseXL_v13.safetensors"
			}
		]
	},
	"sd-xl-turbo-v1-0": {
		"name": "sd-xl-turbo-v1-0",
		"model": "sd_xl_turbo_1.0.safetensors",
		"pipe": diffusers.StableDiffusionXLPipeline,
		"width": 512,
		"height": 512,
		"guidance_scale": [0.0],
		"num_inference_steps": range(1,4),
		"scheduler": [
			#diffusers.LCMScheduler,
			diffusers.EulerAncestralDiscreteScheduler
		],
		"config_dict": {
			"beta_end": 0.012,
			"beta_schedule": "scaled_linear",
			"beta_start": 0.00085,
			"clip_sample": False,
			"interpolation_type": "linear",
			"num_train_timesteps": 1000,
			"prediction_type": "epsilon",
			"sample_max_value": 1.0,
			"set_alpha_to_one": False,
			"skip_prk_steps": True,
			"steps_offset": 1,
			"timestep_spacing": "trailing",
			"trained_betas": None
		},
		"base": [
			{
				"url": "https://huggingface.co/stabilityai/sdxl-turbo/blob/main/sd_xl_turbo_1.0.safetensors",
				"file": "sd_xl_turbo_1.0.safetensors"
			}
		],
		"loras": [
			{
				"url": "",
				"file": ""
			},
		],
		"checkpoints" : [
			{
				"url": "https://civitai.com/models/112902?modelVersionId=251662",
				"file": "dreamshaperXL_turboDpmppSDE.safetensors",
				"cfg": [2.0, 3.0],
				"steps": [5, 7]
			},
			{
				"url": "https://civitai.com/models/139562?modelVersionId=272378",
				"file": "realvisxlV30_v30TurboBakedvae.safetensors",
				"cfg": 1.5,
				"steps": 4
			},
			{
				"url": "https://civitai.com/models/215538?modelVersionId=242881",
				"file": "pixelwaveturboExcellent_01.safetensors",
				"cfg": 2.0,
				"steps": 6
			},
			{
				"url": "https://civitai.com/models/129666?modelVersionId=242718",
				"file": "RealitiesEdgeXLLCM_TURBOXL.safetensors",
				"cfg": 2.0
			}
		]
	}
}