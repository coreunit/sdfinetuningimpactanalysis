from huggingface_hub import login
from datasets import load_dataset

import urllib.request
import pathlib
import json

def main():
	
	# If the dataset is gated/private, make sure you have run huggingface-cli login
	f = open('credentials.json')
	credentials = json.load(f)
	login(token=credentials["huggingface_api_key"])

	for split in ["train", "validation", "test"]:

		# Create a folder to store the images
		output_folder = pathlib.Path("./data") / "imagenet-1k" / split
		output_folder.mkdir(parents=True, exist_ok=True)
		
		# 
		dataset = load_dataset("imagenet-1k", split=split, streaming=True)
		count = 0

		for i, data in enumerate(dataset):
			
			print(data)

			image_path = output_folder / (str(i) + ".png")

			if image_path.exists():
				continue

			image = data["image"] #PIL image
			label = data["label"] #String

			if image.size[0] < 512:
				print("Width to low")
				continue

			if image.size[1] < 512:
				print("Height to low")
				continue
			
			image.save(output_folder / (str(i) + ".png"))
			count += 1

			if count >= 1024:
				break



if __name__ == '__main__':
	
	main()