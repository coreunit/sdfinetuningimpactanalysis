# ___ProjectName___

___ProjectName___ description.

## Integration and APIs

- [Huggingface - Datasets](https://huggingface.co/docs/datasets/index) Datasets is a library for easily accessing and sharing datasets for Audio, Computer Vision, and Natural Language Processing (NLP) tasks. Used for downloadin [Imagenet 1k](https://huggingface.co/datasets/imagenet-1k)

## Installation

### Linux setup:

1. 
```sh
sudo apt update
sudo apt upgrade
sudo apt install git
sudo apt install python3
sudo apt install python3-pip
sudo apt install python3-venv

sudo apt install libxcb-cursor0

sudo apt install ubuntu-drivers-common
driver_version=$(apt search nvidia-driver | grep -oP '^nvidia-driver-\K[0-9]+' | tail -1 2>/dev/null)
sudo apt install nvidia-driver-$driver_version
sudo reboot now
```

Run the following to check your cuda version
```sh
nvidia-smi
cuda_version=$(nvidia-smi | grep -oP 'CUDA Version: \K[0-9.]+')
```



### Windows setup:

1. Install the newest version of [Python 3](https://www.python.org/downloads/)

2. Install the newest version of either [CUDA (Nvidia)](https://developer.nvidia.com/cuda-downloads) or [ROCm (AMD)]() depending on your system, that is PyTorch compatible, as of writing that is CUDA v12.1 and ROCm v5.6 respectivly. You can find the compatible versions for your setup listed on the [Pytorch Website](https://pytorch.org/get-started/locally/) 

3. Get the files, Clone the repository using git or dowload the repo as a zip and extract it.
```bat
git clone https://github.com/___user___/___ProjectName___
cd ___ProjectName___
pip install -r requirements.txt
```

```bat
git clone https://github.com/___user___/___ProjectName___
cd ___ProjectName___
python setup.py install --user
```

3.

Edit the top row in the requirements.txt file, replace the url --extra-index-url according to the specification on the pytorch website, this will tell the pytorch library how to, 

4. Run the install script.

The setup install will create a [virtual enfironment](https://docs.python.org/3/library/venv.html) and install all nessesary python libraries.

## Setup

Create your personal  to the config.ini file by renaming the config.ini.example file to config.ini and updating it to include your credentials.

## Credentials

1. Log in to your hugginface account
2. Got to your (Hugginface Access Token)[https://huggingface.co/settings/tokens] control panel and create a new read only token.
3. Go to the hugginface [Imagenet 1k](https://huggingface.co/datasets/imagenet-1k) repository and accept the terms and conditions.
4. rename credential.json.example to credentials.json
5. edit the file to include the newly created access token.

## Run

1. Run the download script to download all the real images, the images are the first 1024 images of the imagenet dataset that exceed 512x512 in size.
2. Run the Generate script to generate all the fake images, images are generate using the caption of the real images as prompts, captioning is done using [BLIP](https://huggingface.co/Salesforce/blip-image-captioning-base)