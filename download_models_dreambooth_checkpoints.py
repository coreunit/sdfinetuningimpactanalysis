from huggingface_hub import login
from datasets import load_dataset

import urllib.parse
import urllib.request
import pathlib
import json

from config import config

def main():
	
	# You will have to download these manually for now
	# TODO Implement CivitAi authentication

	for model_name, model in config.items():
		None
		for checkpoint in model["checkpoints"]:

			input("Press any key")

			parsed_url = urllib.parse.urlparse(checkpoint["url"])
			query_param = urllib.parse.parse_qs(parsed_url.query)

			file_path = None
			download_link = "https://civitai.com/api/download/models/" + query_param["modelVersionId"][0]
			
			print(download_link)
		
			# Check if file is already downloaded
			request = urllib.request.Request(download_link, method="HEAD")
			with urllib.request.urlopen(request) as response:
				
				file_name = response.info().get_filename()
				print(file_name)
				
				file_path = pathlib.Path("checkpoints") / model_name / filename
				if file_path.exists():
					continue
			
			continue
			# Download file
			request = urllib.request.Request(download_link)
			with urllib.request.urlopen(request) as response:
				
				with open(file_path, "wb") as out_file:
					data = response.read()
					out_file.write(data)




if __name__ == '__main__':
	
	main()