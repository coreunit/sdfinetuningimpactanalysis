import PIL
import PIL.PngImagePlugin

import pathlib
import transformers
import random
import json

import generate_images
from config import config

def main():

	print("Loading BLIP")

	processor = transformers.AutoProcessor.from_pretrained("Salesforce/blip-image-captioning-base")
	blip_model = transformers.BlipForConditionalGeneration.from_pretrained("Salesforce/blip-image-captioning-base").to("cuda")
	
	for split in ["train", "validation", "test"]:

		captions_data = {}

		# Create folders
		real_folder = pathlib.Path("./data/imagenet-1k") / split

		for file_path in real_folder.glob("*"):

			# Image caption
			image = PIL.Image.open(file_path)
			caption = generate_images.blip_caption(processor, blip_model, file_path)

			# Save filename-caption pairing
			captions_data[file_path.stem] = {
				"size" : image.size,
				"caption" : caption
			}

		# Save captions to a JSON file
		json_filename = pathlib.Path("./data/imagenet-1k") / (split + ".json")
		with open(json_filename, "w") as json_file:
			json.dump(captions_data, json_file, indent="\t")

if __name__ == '__main__':

	main()