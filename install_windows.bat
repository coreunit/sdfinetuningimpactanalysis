@echo off

REM Check if Python is installed
where python >nul 2>nul
if %errorlevel% equ 0 (
    echo Python is installed.
) else (
    echo Python is not installed. Exiting.
    exit /b 1
)

REM Check if Pip is installed
where pip >nul 2>nul
if %errorlevel% equ 0 (
    echo Pip is installed.
) else (
    echo Pip is not installed. Exiting.
    exit /b 1
)

REM Check if Git is installed
where git >nul 2>nul
if %errorlevel% equ 0 (
    echo Git is installed.
) else (
    echo Git is not installed. Exiting.
    exit /b 1
)

rem Clone the Git repository
rem git clone https://github.com/user/example.git

rem Create a virtual environment
py -m venv .\venv

rem Activate the virtual environment
rem .\venv\Scripts\activate

rem Upgrade pip
.\venv\Scripts\python.exe -m pip install --upgrade pip

rem Install/upgrade dependencies from requirements.txt
.\venv\Scripts\pip.exe install --upgrade -r requirements.txt

rem Deactivate the virtual environment
rem deactivate

rem Display a message and wait for user input
pause