#!/bin/bash

# Check if Python is installed
if command -v python3 &>/dev/null; then
    echo "Python is installed."
else
    echo "Python is not installed. Exiting."
    exit 1
fi

# Check if Pip is installed
if command -v pip3 &>/dev/null; then
    echo "Pip is installed."
else
    echo "Pip is not installed. Exiting."
    exit 1
fi

# Check if Git is installed
if command -v git &>/dev/null; then
    echo "Git is installed."
else
    echo "Git is not installed. Exiting."
    exit 1
fi

# Clone the Git repository
# git clone https://github.com/user/example.git

# Create a virtual environment
python3 -m venv venv

# Activate the virtual environment
# source venv/bin/activate

# Upgrade pip
venv/bin/python -m pip install --upgrade pip

# Install/upgrade dependencies from requirements.txt
venv/bin/pip install --upgrade -r requirements.txt

# Deactivate the virtual environment
# deactivate

# Display a message and wait for user input
read -p "Press Enter to exit"