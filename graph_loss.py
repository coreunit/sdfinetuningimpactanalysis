import json
import matplotlib.pyplot as plt
import pathlib
import numpy as np
from config import config

model_id = "sd-xl-base-v1-0"
#model_id = "sd-xl-turbo-v1-0"
#model_id = "sd-v1-5"
detector_folder = pathlib.Path("./detector") / model_id

def plot_loss_from_json(json_path):
	with open(json_path, 'r') as file:
		data = json.load(file)

	epochs = data["epoch"]
	train_loss = data["training_loss"]
	validation_loss = data["validation_loss"]
	
	plt.figure(figsize=(8, 8))
	#plt.gca().set_aspect('equal', adjustable='box')

	plt.plot(epochs, train_loss, label='Training Loss')
	plt.plot(epochs, validation_loss, label='Validation Loss')
	
	min_val_loss_epoch = np.argmin(validation_loss)
	min_val_loss = validation_loss[min_val_loss_epoch]

	#plt.scatter(min_val_loss_epoch, min_val_loss, color='red', label=f'Early Cutoff Epoch: {min_val_loss_epoch}')
	plt.plot([min_val_loss_epoch, min_val_loss_epoch], [0, 1], linestyle='--', color='red', label=f'Min Validation Loss Epoch: {min_val_loss_epoch}')

	plt.xlim([-0.05, 30.05])
	plt.ylim([-0.05, 1.05])
	plt.xlabel('Epochs')
	plt.ylabel('Loss')
	plt.title('Training and Validation Loss Over Epochs')
	plt.legend(loc='upper right')
	plt.show()

# Replace 'path/to/your/loss.json' with the actual path to your loss JSON file
json_file_path = detector_folder / "loss.json"

if json_file_path.is_file():
	plot_loss_from_json(json_file_path)
else:
	print(f"Error: The file '{json_file_path}' does not exist.")