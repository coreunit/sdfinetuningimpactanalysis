import torch
import torch.nn as nn
import torch.optim as optim

from torch.utils.data import DataLoader

from custom_data_loader import CustomDataset
from model_vgg16_binary import VGG16BinaryClassifier

import json
import pathlib
from config import config

def process_epoch(model, data_loader, optimizer, criterion, device, backward_propagation=True):
	if backward_propagation:
		model.train()
	else:
		model.eval()

	epoch_loss = 0.0
	num_steps = 0
	for inputs, labels in data_loader:

		print("S:", num_steps, end="\r")
		num_steps += 1

		inputs, labels = inputs.to(device), labels.to(device)

		# Forward pass
		outputs = model(inputs)
		labels = labels.float().view(-1, 1)
		loss = criterion(outputs, labels)

		# Backward pass and optimization
		if backward_propagation:
			optimizer.zero_grad()
			loss.backward()
			optimizer.step()

		# Metrics
		epoch_loss += loss.item()

	# Calculate average loss for the epoch
	average_loss = epoch_loss / len(data_loader)

	return average_loss

def main():

	# Set device (use GPU if available)
	device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

	# Define your VGG16BinaryClassifier
	classifier = VGG16BinaryClassifier().to(device)
	
	#model_id = "sd-xl-turbo-v1-0"
	model_id = "sd-v1-5"
	real_images_folder = pathlib.Path("./data") / "imagenet-1k"
	fake_images_folder = pathlib.Path("./data") / model_id
	detector_folder = pathlib.Path("./detector") / model_id
	detector_folder.mkdir(parents=True, exist_ok=True)

	subset = "train"
	training_dataset = CustomDataset(real_images_folder / subset, fake_images_folder / subset)
	
	subset = "validation"
	validation_dataset = CustomDataset(real_images_folder / subset, fake_images_folder / subset)

	# Create data loaders
	train_batch_size = 32
	val_batch_size = 32

	train_data_loader = DataLoader(training_dataset, batch_size=train_batch_size, shuffle=True, num_workers=4)
	val_data_loader = DataLoader(validation_dataset, batch_size=val_batch_size, shuffle=False, num_workers=4)

	# Define loss function and optimizer
	criterion = nn.BCELoss()
	optimizer = optim.SGD(classifier.parameters(), lr=0.001, momentum=0.9)
	scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10, verbose=True)

	# Training loop
	num_epochs = 100
	validation_loss_old = float('inf')

	for epoch in range(num_epochs):

		print(f"Epoch: {epoch + 1}")

		# Training
		training_loss = process_epoch(classifier, train_data_loader, optimizer, criterion, device, backward_propagation=True)
		print(f"Training Loss: {training_loss:.4f}")

		# Validation
		validation_loss = process_epoch(classifier, val_data_loader, None, criterion, device, backward_propagation=False)
		print(f"Validation Loss: {validation_loss:.4f}")

		# Scheduler step based on validation loss
		scheduler.step(validation_loss)

		# Save loss to the JSON file for later plotting
		data = {"epoch": [], "training_loss": [], "validation_loss": []}
		existing_data = None

		loss_log_path = detector_folder / "loss.json"

		try:
			with open(loss_log_path, 'r') as file:
				existing_data = json.load(file)
		except (json.JSONDecodeError, FileNotFoundError):
			existing_data = {"epoch": [], "training_loss": [], "validation_loss": []}

		# Append new data
		data["epoch"] = existing_data.get("epoch", [])
		data["training_loss"] = existing_data.get("training_loss", [])
		data["validation_loss"] = existing_data.get("validation_loss", [])

		data["epoch"].append(epoch)
		data["training_loss"].append(training_loss)
		data["validation_loss"].append(validation_loss)

		with open(loss_log_path, 'w+') as file:
			json.dump(data, file, indent="\t")
		
		if validation_loss < validation_loss_old:

			validation_loss_old = validation_loss

			# Remove old classifier
			for file_path in detector_folder.glob("vgg16_binary_classifier_e*.pth"):
				file_path.unlink()

			# Save itteration
			torch.save(classifier.state_dict(), detector_folder / ("vgg16_binary_classifier_e"+str(epoch)+".pth"))


if __name__ == "__main__":
	main()