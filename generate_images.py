import torch
import diffusers

import PIL
import PIL.PngImagePlugin

import random
import pathlib

from config import config

def adjust_aspect_ratio(shape_a, shape_b):
	# Unpack shapes A and B
	width_a, height_a = shape_a
	width_b, height_b = shape_b

	# Calculate the aspect ratio of shapes A and B
	aspect_ratio_a = width_a / height_a
	aspect_ratio_b = width_b / height_b

	# Determine the scaling factor based on aspect ratios
	scaling_factor = aspect_ratio_b / aspect_ratio_a

	# Adjust dimensions of shape A to be as close to the aspect ratio of shape B while still being divisible by 8
	width_a = int(round(width_a * scaling_factor / 8) * 8) if scaling_factor > 1.0 else width_a
	height_a = int(round(height_a / scaling_factor / 8) * 8) if scaling_factor < 1.0 else height_a

	# Limit due to 24GB VRAM requirement for SDXL model
	width_a = min(width_a, 3072)
	height_a = min(height_a, 3072)

	print(shape_a, shape_b, (width_a, height_a))
	return width_a, height_a


def blip_caption(processor, blip_model, image_path):

	# Generate caption of real image for use as prompt
	text = "a photograph of"
	image = PIL.Image.open(image_path)
	inputs = processor(image, text, return_tensors="pt").to("cuda")
	output = blip_model.generate(**inputs)
	caption = processor.decode(output[0], skip_special_tokens=True)

	return caption

def func2(model_id, pipeline, prompt, output_dir, shape):

	width = config[model_id]["width"]
	height = config[model_id]["height"]
	width, height = adjust_aspect_ratio((width, height), shape)

	seed = random.randint(0, 9999999999)
	
	guidance_scale = random.choice(config[model_id]["guidance_scale"])
	num_inference_steps = random.choice(config[model_id]["num_inference_steps"])
	
	# TODO implement correct overide of guidance scale and inference step for LoRAs and Dreambooth
	# This is hardcoded overide
	#guidance_scale = 1.0
	#num_inference_steps = 12

	# Select scheduler
	chosen_scheduler= random.choice(config[model_id]["scheduler"])
	
	if "config_dict" in config[model_id]:
		pipeline.scheduler = chosen_scheduler.from_config(config[model_id]["config_dict"])
	else:
		pipeline.scheduler = chosen_scheduler.from_config(pipeline.scheduler.config)
	
	print(width, height, prompt)

	output = pipeline(
		prompt=prompt,
		negative_prompt="",
		#width=512,
		width=width,
		#height=512,
		height=height,
		guidance_scale=guidance_scale,
		num_inference_steps=num_inference_steps,
		#cross_attention_kwargs={"scale": 1.0},
		generator=torch.Generator(device="cuda").manual_seed(seed),
	)

	metadata = PIL.PngImagePlugin.PngInfo()
	metadata.add_text("seed", str(seed))
	metadata.add_text("prompt", str(prompt))
	metadata.add_text("negative_prompt", str(""))
	metadata.add_text("guidance_scale", str(guidance_scale))
	metadata.add_text("num_inference_steps", str(num_inference_steps))
	metadata.add_text("scheduler", str(chosen_scheduler.__class__.__name__))

	# Save image
	output.images[0].save(output_dir, pnginfo=metadata)

	# 
	#file_path = pathlib.Path(output_dir)
	#image2 = PIL.Image.open(file_path)
	#print(image2.getexif())

def main():

	None

if __name__ == '__main__':

	main()