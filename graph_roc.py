import torch
from torch.utils.data import DataLoader
from custom_data_loader import CustomDataset
from model_vgg16_binary import VGG16BinaryClassifier
import pathlib
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score, roc_curve, auc

import numpy as np
import json
from config import config

def test(classifier, data_loader, device, graph_path):

	classifier.eval()

	true_labels = []
	predicted_probs = []

	with torch.no_grad():

		for inputs, labels in data_loader:
			
			# Move data to device
			inputs = inputs.to(device)
			labels = labels.to(device)

			# Forward pass
			outputs = classifier(inputs)
			labels = labels.float().view(-1, 1)

			true_labels.extend(labels.cpu().numpy())
			predicted_probs.extend(outputs.cpu().numpy())	

	# ROC Curve
	fpr, tpr, thresholds = roc_curve(true_labels, predicted_probs)

	#youden_index = (tpr - fpr).max()
	optimal_idx = (tpr - fpr).argmax()
	optimal_threshold = thresholds[optimal_idx]

	print("Optimal threshold:", optimal_threshold)

	roc_auc = auc(fpr, tpr)

	# Print AUC
	print(f'Area under curve: {roc_auc:.4f}')

	# Plot ROC Curve
	plt.figure(figsize=(8, 8))
	plt.gca().set_aspect('equal', adjustable='box')
	plt.plot(fpr, tpr, color='darkorange', lw=2, label='ROC curve (area = {:.2f})'.format(roc_auc))
	plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
	plt.xlim([-0.05, 1.05])
	plt.ylim([-0.05, 1.05])
	plt.xlabel('False Positive Rate')
	plt.ylabel('True Positive Rate')
	plt.title('Receiver Operating Characteristic Curve')
	plt.legend(loc='lower right')

	# Plot point
	plt.scatter(fpr[optimal_idx], tpr[optimal_idx], c='green', marker='o', label=f'Optimal Threshold = {optimal_threshold:.2f}')
	plt.legend(loc='lower right')

	#plt.show()
	plt.savefig(graph_path)
	plt.close()

	# Calculate metrics
	predicted_labels = (np.array(predicted_probs) >= optimal_threshold)
	
	metrics_dict = {
		"confusion_matrix": confusion_matrix(true_labels, predicted_labels).tolist(),
		"accuracy": accuracy_score(true_labels, predicted_labels),
		"precision": precision_score(true_labels, predicted_labels),
		"recall": recall_score(true_labels, predicted_labels),
		"f1_score": f1_score(true_labels, predicted_labels)
	}

	print(metrics_dict)

	json_file_path = graph_path.replace('.png', '.json')
	with open(json_file_path, 'w+') as json_file:
		json.dump(metrics_dict, json_file, indent='\t')

def main():

	model_id = "sd-xl-turbo-v1-0"

	device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
	classifier = VGG16BinaryClassifier().to(device)

	for version in ["", "-lora", "-checkpoint"]:

		#Load images
		real_folder = pathlib.Path("./data/imagenet-1k") / "test"
		fake_folder = pathlib.Path("./data") / (model_id + version) / "test"
		custom_dataset = CustomDataset(real_folder, fake_folder)

		# Load the saved model
		detector_folder = pathlib.Path("./detector") / model_id
		detector_path = next(detector_folder.glob("*.pth"))
		detector = torch.load(detector_path)
		classifier.load_state_dict(detector)
		classifier.eval()

		# Create data loader for testing
		batch_size = 1
		test_data_loader = DataLoader(custom_dataset, batch_size=batch_size, shuffle=False, num_workers=4)

		# Test the trained model
		graph_path = (model_id + version) + "-roc.png"
		test(classifier, test_data_loader, device, graph_path)

if __name__ == '__main__':
	main()


	
