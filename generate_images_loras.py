import PIL
import PIL.PngImagePlugin

import pathlib
import transformers
import random
import json

from config import config
import generate_images

def main():

	print("Loading SD")

	model_id = "sd-xl-turbo-v1-0"
	lora_path = pathlib.Path("./loras") / model_id
	modelpath = pathlib.Path("./models") / model_id / config[model_id]["model"]
	pipeline = config[model_id]["pipe"].from_single_file(str(modelpath)).to("cuda")

	for split in ["test"]:

		# Create folders
		real_folder = pathlib.Path("./data") / "imagenet-1k" / split
		fake_folder = pathlib.Path("./data") / (model_id + "-" + "lora") / split
		fake_folder.mkdir(parents=True, exist_ok=True)

		real_folder_files = real_folder.glob("*")
		real_folder_files_count = len(list(real_folder_files))
		
		lora_id = None
		lora_files = list(lora_path.glob("*"))
		lora_files_count = len(list(lora_files))

		json_file = pathlib.Path("./data/imagenet-1k") / (split + ".json")
		image_data = None
		with open(json_file) as json_data:
			image_data = json.load(json_data)

		for index, file_path in enumerate(real_folder.glob("*")):

			fake_path = fake_folder / (file_path.stem + ".png")
			if fake_path.exists():
				continue
			
			new_lora_id = int(index//(real_folder_files_count/lora_files_count))

			if lora_id != new_lora_id:
				lora_id = new_lora_id

				lora = lora_files[lora_id]

				print(lora.name)

				pipeline.unload_lora_weights()
				pipeline.load_lora_weights(lora_path / lora.name, adapter_name=lora.stem)
				pipeline.set_adapters([lora.stem], adapter_weights=[1.0])

			image_prompt = image_data[file_path.stem]["caption"]
			image_size = image_data[file_path.stem]["size"]

			# Image generation
			generate_images.func2(model_id, pipeline, image_prompt, fake_path, image_size)

if __name__ == '__main__':

	main()