import torch
import torch.nn as nn

class ConvBinaryClassifier(nn.Module):
	def __init__(self):
		super(ConvBinaryClassifier, self).__init__()

		# Define convolutional layers
		self.conv_layers = nn.Sequential(
			nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1),
			nn.Tanh(inplace=True),
			nn.Conv2d(64, 64, kernel_size=2, stride=2, padding=0),
			nn.Tanh(inplace=True),

			nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
			nn.Tanh(inplace=True),
			nn.Conv2d(128, 128, kernel_size=2, stride=2, padding=0),
			nn.Tanh(inplace=True),

			nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1),
			nn.Tanh(inplace=True),
			nn.Conv2d(256, 256, kernel_size=2, stride=2, padding=0),
			nn.Tanh(inplace=True),

			nn.Conv2d(256, 512, kernel_size=3, stride=1, padding=1),
			nn.Tanh(inplace=True),
			nn.Conv2d(512, 512, kernel_size=2, stride=2, padding=0),
			nn.Tanh(inplace=True),

			nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1),
			nn.Tanh(inplace=True),
			nn.Conv2d(512, 512, kernel_size=2, stride=2, padding=0),
			nn.Tanh(inplace=True)
		)

		# Define a new classifier for binary classification
		self.classifier = nn.Sequential(
			nn.Linear(512 * 8 * 8, 4096),
			nn.Tanh(inplace=True),
			nn.Dropout(),
			nn.Linear(4096, 4096),
			nn.Tanh(inplace=True),
			nn.Dropout(),
			nn.Linear(4096, 1),  # Output layer for binary classification
			nn.Sigmoid()        # Sigmoid activation for binary classification
		)

	def forward(self, x):
		x = self.conv_layers(x)
		x = x.view(x.size(0), -1)  # Flatten the output for the fully connected layers
		x = self.classifier(x)
		return x

if __name__ == '__main__':

	classifier = ConvBinaryClassifier()
	print(classifier)
