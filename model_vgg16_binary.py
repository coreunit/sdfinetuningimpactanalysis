import torch
import torch.nn as nn
import torchvision.models as models

# Define the VGG16 model for binary classification
class VGG16BinaryClassifier(nn.Module):
	
	def __init__(self):
		super(VGG16BinaryClassifier, self).__init__()
		
		# Load pre-trained VGG16 model
		vgg16 = models.vgg16(pretrained=True)
		
		# Remove the last fully connected layer (classifier)
		self.features = vgg16.features
		
		# Define a new classifier for binary classification
		self.classifier = nn.Sequential(
			nn.Linear(512 * 8 * 8, 4096),
			nn.ReLU(inplace=True),
			nn.Dropout(),
			nn.Linear(4096, 4096),
			nn.ReLU(inplace=True),
			nn.Dropout(),
			nn.Linear(4096, 1),  # Output layer for binary classification
			nn.Sigmoid()         # Sigmoid activation for binary classification
		)

	def forward(self, x):
		x = self.features(x)
		x = x.view(x.size(0), -1)  # Flatten the output for the fully connected layers
		x = self.classifier(x)
		return x

if __name__ == '__main__':
	
	classifier = VGG16BinaryClassifier()
	print(classifier)