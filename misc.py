def gen():

	#modelpath = pathlib.Path("./models") / "v1-5-pruned.ckpt"
	#modelpath = pathlib.Path("./models") / "revAnimated_v122.safetensors"
	modelpath = pathlib.Path("./models") / "sd_xl_base_1.0.safetensors"

	#pipeline = DiffusionPipeline.from_pretrained(str(modelpath),  use_safetensors=True).to("cuda")
	#pipeline = diffusers.StableDiffusionPipeline.from_single_file(str(modelpath)).to("cuda")
	pipeline = diffusers.StableDiffusionXLPipeline.from_single_file(str(modelpath)).to("cuda")
	#pipeline = diffusers.StableDiffusionSAGPipeline.from_single_file(str(modelpath)).to("cuda")
	#pipeline.scheduler = diffusers.UniPCMultistepScheduler.from_config(pipeline.scheduler.config)
	pipeline.scheduler = diffusers.EulerDiscreteScheduler.from_config(pipeline.scheduler.config)
	#pipeline.load_lora_weights(".", weight_name="howls_moving_castle.safetensors")

	prompt = "photography of a black cube in a fishbowl"
	negative_prompt = ""

	output = pipeline(
		prompt=prompt,
		negative_prompt=negative_prompt,
		width=512,
		height=512,
		num_inference_steps=2,
		guidance_scale=1,
		#num_images_per_prompt=1,
		#generator=torch.manual_seed(0)
	)

	output.images[0].save("./test.png")


def get_metadata():

	import json

	filename = "more_details.safetensors"
	with open(filename, 'rb') as f:
		header_len = int.from_bytes(f.read(8), 'little')
		header_data = f.read(header_len).decode('utf-8')
		header = json.loads(header_data)
		print(header)
		if header is not None and "__metadata__" in header:
			metadata = header["__metadata__"]
			print("Metadata:", metadata)
		else:
			print("No metadata found.")

#print(dir(image))

#if 0.5 > image.shape[0] / image.shape[1] > 2.0:
#	print("Not square enough")

#if image.shape[0] < 512:
#	print("Width to low")

#if image.shape[1] < 512:
#	print("Height to low")

#if image.shape[0] > 512 
#image_path = output_folder / (str(item["label"]) + "." + str(image.format).lower())

#if image_path.exists()
#	print
#print(image_path)

#image.save(image_path)

#input()

#print("Post filter")
'''
for image in subset:
	
	image_path = output_folder / (label + ".jpg")
	image.save(image_path)
	print(f"Saved image for class {label} to {image_path}")
	exit()
'''


def calculate_metrics(true_labels, predicted_labels):
	true_positives = sum(true_label == 1 and predicted_label == 1 for true_label, predicted_label in zip(true_labels, predicted_labels))
	true_negatives = sum(true_label == 0 and predicted_label == 0 for true_label, predicted_label in zip(true_labels, predicted_labels))
	false_positives = sum(true_label == 0 and predicted_label == 1 for true_label, predicted_label in zip(true_labels, predicted_labels))
	false_negatives = sum(true_label == 1 and predicted_label == 0 for true_label, predicted_label in zip(true_labels, predicted_labels))

	accuracy = (true_positives + true_negatives) / len(true_labels)
	precision = true_positives / (true_positives + false_positives) if true_positives + false_positives != 0 else 0
	recall = true_positives / (true_positives + false_negatives) if true_positives + false_negatives != 0 else 0
	f1 = 2 * (precision * recall) / (precision + recall) if precision + recall != 0 else 0

	return accuracy, precision, recall, f1

def test(classifier, data_loader, device):
	classifier.eval()
	true_labels = []
	predicted_labels = []

	with torch.no_grad():
		for inputs, labels in data_loader:
			inputs, labels = inputs.to(device), labels.to(device)

			# Forward pass
			outputs = classifier(inputs)
			labels = labels.float().view(-1, 1)

			# Metrics
			predictions = (outputs >= 0.5).float()

			true_labels.extend(labels.cpu().numpy())
			predicted_labels.extend(predictions.cpu().numpy())

	true_labels = [int(label) for sublist in true_labels for label in sublist]
	predicted_labels = [int(label) for sublist in predicted_labels for label in sublist]

	# Calculate confusion matrix manually
	confusion_matrix = [[sum((t, p) == (1, 1) for t, p in zip(true_labels, predicted_labels)),
						 sum((t, p) == (1, 0) for t, p in zip(true_labels, predicted_labels))],
						[sum((t, p) == (0, 1) for t, p in zip(true_labels, predicted_labels)),
						 sum((t, p) == (0, 0) for t, p in zip(true_labels, predicted_labels))]]

	accuracy, precision, recall, f1 = calculate_metrics(true_labels, predicted_labels)

	print(f'Confusion Matrix:\n{confusion_matrix}')
	print(f'Test Accuracy: {accuracy:.4f}')
	print(f'Precision: {precision:.4f}')
	print(f'Recall: {recall:.4f}')
	print(f'F1 Score: {f1:.4f}')