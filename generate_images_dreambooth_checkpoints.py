import PIL
import PIL.PngImagePlugin

import pathlib
import transformers
import random
import json
import torch

from config import config
import generate_images

def main():

	model_id = "sd-xl-turbo-v1-0"
	checkpoint_path = pathlib.Path("./checkpoints") / model_id
	checkpoint_list = list(checkpoint_path.glob("*"))
	checkpoint_id = None

	pipeline = None
	
	for split in ["test"]:

		# Create folders
		real_folder = pathlib.Path("./data") / "imagenet-1k" / split
		fake_folder = pathlib.Path("./data") / (model_id + "-" + "checkpoint") / split
		fake_folder.mkdir(parents=True, exist_ok=True)
		
		real_folder_len = len(list(real_folder.glob("*")))

		json_file = pathlib.Path("./data/imagenet-1k") / (split + ".json")
		image_data = None
		with open(json_file) as json_data:
			image_data = json.load(json_data)
		
		for index, file_path in enumerate(real_folder.glob("*")):
			
			new_checkpoint_id = int(index//(real_folder_len/len(checkpoint_list)))

			if checkpoint_id != new_checkpoint_id:
				checkpoint_id = new_checkpoint_id
				
				model_path = checkpoint_path / checkpoint_list[checkpoint_id].name
				print("Loading SD:", model_path.name)
				pipeline = None
				pipeline = config[model_id]["pipe"].from_single_file(str(model_path)).to("cuda")
				
				print(dir(pipeline))


			fake_path = fake_folder / (file_path.stem + ".png")
			if fake_path.exists():
				continue

			image_prompt = image_data[file_path.stem]["caption"]
			image_size = image_data[file_path.stem]["size"]

			# Image generation
			generate_images.func2(model_id, pipeline, image_prompt, fake_path, image_size)

if __name__ == '__main__':

	main()