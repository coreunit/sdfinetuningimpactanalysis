import io
import random
import pathlib
from PIL import Image, ImageOps

from torch.utils.data import Dataset
from torchvision import transforms

class CustomDataset(Dataset):
	def __init__(self, real_folder, fake_folder):
		self.real_images = list(pathlib.Path(real_folder).glob('*'))
		self.fake_images = list(pathlib.Path(fake_folder).glob('*'))

		# Pair each image with its corresponding label
		self.data = [(img, 1) for img in self.real_images] + [(img, 0) for img in self.fake_images]

	def __len__(self):
		return len(self.data)

	def __getitem__(self, idx):
		img_path, label = self.data[idx]
		image = Image.open(img_path).convert("RGB")

		# Resize to fill a 256x256 bounding box without preserving aspect ratio
		image = ImageOps.fit(image, (256, 256), centering=(0.5, 0.5))
		image = ImageOps.exif_transpose(image)

		# Random JPEG or WebP compression
		image_format = random.choice(["JPEG", "WebP"])
		image_quality = random.randint(50, 100)

		# Apply
		buffer = io.BytesIO()
		image.save(buffer, format=image_format, quality=image_quality)
		image = Image.open(io.BytesIO(buffer.getvalue()))
		
		# Transformations to apply to the images
		transform = transforms.Compose([
			transforms.ToTensor(),
			transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5]),
			transforms.RandomHorizontalFlip(),
			transforms.RandomVerticalFlip(),
			#transforms.RandomRotation(degrees=45),
		])

		image = transform(image)
	
		return image, label