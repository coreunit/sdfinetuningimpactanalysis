import PIL
import PIL.PngImagePlugin

import pathlib
import transformers
import random
import json

from config import config
import generate_images

def main():

	print("Loading SD")

	model_id = "sd-xl-base-v1-0"
	#model_id = "sd-v1-5"
	modelpath = pathlib.Path("./models") / model_id / config[model_id]["model"]
	pipeline = config[model_id]["pipe"].from_single_file(str(modelpath)).to("cuda")
	
	for split in ["train", "validation", "test"]:

		# Create folders
		real_folder = pathlib.Path("./data/imagenet-1k") / split
		fake_folder = pathlib.Path("./data") / model_id / split
		fake_folder.mkdir(parents=True, exist_ok=True)

		json_file = pathlib.Path("./data/imagenet-1k") / (split + ".json")
		image_data = None
		with open(json_file) as json_data:
			image_data = json.load(json_data)
		
		for index, file_path in enumerate(real_folder.glob("*")):

			fake_path = fake_folder / (file_path.stem + ".png")
			if fake_path.exists():
				continue

			image_prompt = image_data[file_path.stem]["caption"]
			image_size = image_data[file_path.stem]["size"]

			# Image generation
			generate_images.func2(model_id, pipeline, image_prompt, fake_path, image_size)

if __name__ == '__main__':

	main()